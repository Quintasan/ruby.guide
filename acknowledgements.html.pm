#lang pollen

◊(define-meta title "Acknowledgements")
◊(define-meta subtitle "People who made it easier")
◊(define-meta published "2019-04-12T00:00:00+01:00")
◊(define-meta updated "2020-01-15T20:07:32+01:00")
◊(define-meta uuid "b1aaa891-a7c6-4bf4-a01c-4c43c0446abf")

◊(clear-sidenotes)

I would like to extend my thanks to everyone who has paid for this book. Your payment, no matter how big, gives me encouragement to keep working on this book.

I would like to thank my friend, Paweł, who kind of pushed me into actually doing this.

A big thank you to Yukihiro Matsumoto for creating Ruby back in 1993 so that I could discover it two decades later.

I welcome suggestions for future revisions of this book.
