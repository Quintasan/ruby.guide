#lang racket/base

(require "links.rkt")
(require "tags.rkt")

(provide (all-defined-out))

(define main-title "Ruby Guide")
(define subtitle "Because the world clearly needs another one!")
(define description "Introduction to programming using Ruby for dummies")

(define subscription-form
    `(form ((name "follow")
            (method "post")
            (target "_blank")
            (class "subscribe")
            (data-netlify "true")
            (netlify-honeypot "bot-field"))
        (input ((placeholder "Do not fill this out if you're a human")
                (name "bot-field")
                (style "display: none;")))
        (input ((placeholder "Enter your email to join the mailing list")
                (id "email")
                (class "email")
                (value "")
                (type "email")
                (name "email")))
        (input ((id "subscribe")
                (class "submit")
                (type "submit")
                (value "Subscribe")))
            ))

(define follow-section
  `(section((class "follow"))
     (div ((class "wip"))
          "The book is a work in progress. If you want to keep up to date:")
     ,subscription-form
     (div ((class "links"))
          "Subscribe to the " ,(link feed-url "feed")
          ", see the source on " ,(link source-code "GitLab")
          " or " ,(link mailto "contact me")
          ".")
     (div ((class "donate"))
           "If you appreciate this free book please consider "
           ,(link donations "donating")
           ".")
     ))

(define (abs-url url)
  (format "~a/~a" root-url url))

(define keywords
  "Ruby,Guide,Programming,Howto,Rails,Tutorial")

