#lang pollen

◊(define-meta title "Recommended readings")
◊(define-meta subtitle "One book is never enough")
◊(define-meta published "2019-04-12T00:00:00+01:00")
◊(define-meta updated "2020-01-15T20:07:32+01:00")
◊(define-meta uuid "b1aaa891-a7c6-4bf4-a01c-4c43c0446abf")

◊(clear-sidenotes)

◊epigraph{
  ◊qt[#:author "Isaac Newton"
      #:src "in a letter to Robert Hooke"
      #:url hsp]{
    If I have seen further it is by standing on the shoulders of Giants.
  }
}

◊(define hsp
  (x-ref
    "2020-07-29"
    "https://discover.hsp.org/Record/dc-9792"
    "Historical Society of Pennsylvania: Isaac Newton letter to Robert Hooke, 1675"
    ))

To be perfectly honest, this is my first book. As such, you sometimes might be confused by the things that I have written. If this happens then it means that I have failed in making things simple and understandable, do not hestiate about letting me know about that. But please do not wait for my response for there's a plethora of materials available to you at no cost when it comes to Ruby. Below I have compiled a list of other books that you can use as an alternative to this one. Whenever possible I will provide links to the relevant chapters so that you don't have to skim the whole thing.

The list is as follows, in no particular order:

◊ol{
  ◊li{◊(book-link learn-to-program) by Chris Pine}
  ◊li{◊(book-link learn-ruby-the-hard-way) by Zed A. Shaw}
  ◊li{◊(book-link whys-guide-to-ruby) by why the lucky stiff}
  ◊li{◊link[ruby-koans]{Ruby Koans} by Jim Weirich}
}

I would like to thank authors of those books as I have also used them in the past and some examples I give here might be influenced by them. Please support them buy buying their books if possible.
