#lang pollen

◊(define-meta title "On editors")
◊(define-meta subtitle "The tool of programmer's trade")
◊(define-meta published "2019-04-12T00:00:00+01:00")
◊(define-meta updated "2020-01-15T20:07:32+01:00")
◊(define-meta uuid "b1aaa891-a7c6-4bf4-a01c-4c43c0446abf")

◊(clear-sidenotes)

◊epigraph{
  ◊qt[#:author "jclancy"
      #:src "How do I exit the Vim editor? on Stack Overflow"
      #:url how-do-i-exit-vim]{
    I'm stuck and cannot escape. It says:
    ◊(code "type :quit<Enter> to quit VIM")
    But when I type that it simply appears in the object body.
  }
}

◊(define how-do-i-exit-vim
  (x-ref
    "2020-08-08"
    "https://stackoverflow.com/questions/11828270/how-do-i-exit-the-vim-editor"
    "vi - How do I exit the Vim editor? - Stack Overflow"))

◊(define editor-war-wikipedia
  (x-ref
    "2020-08-80"
    "https://en.wikipedia.org/wiki/Editor_war"
    "Editor war - Wikipedia"))

◊(define vscode-ruby-extension
  (x-ref
    "2021-03-04"
    "https://marketplace.visualstudio.com/items?itemName=rebornix.Ruby"
    "Ruby language support and debugging for Visual Studio Code"))

◊(define visual-studio-code
  (x-ref
    "2020-08-08"
    "https://code.visualstudio.com/"
    "Visual Studio Code homepage"))

tl;dr Install ◊link[visual-studio-code]{Visual Studio Code} and ◊link[vscode-ruby-extension]{Ruby extension}

The basic tool of a programmer is a text editor. Some people might say that you need an IDE like JetBrains RubyMine to be a programmer but that's not true. Essentially, there are only two things that you need to be a programmer:

◊ol{
  ◊li{Editor of ◊strong{your} choice}
  ◊li{A way to run your program - in our case Ruby interpreter}
}

Many words have been spilled in the eternal debate over which editor is the superior choice for the master programmer. There's a ◊link[editor-war-wikipedia]{Wikipedia article} dedicated to one of the longest running holy wars in the history of programming if you are interested in people arguing over such a thing. This book wouldn't be complete if I didn't mention this.

I have written this book assuming that the reader has basic proficiency in using the computer. By this I mean you are comfortable in installing additional software on your computer, you can perform Google search and know how to use a text editor. However I will not assume you are comfortable using the terminal (there will be a separate chapter for this) or have the slightest idea of what modal editing is (there won't be a chapter for this). If any of the editor crusaders approach you and try to convince you that you should use their favourite editor, please kindly tell them to go back where they came from and that you're comfortable enough with your current editor.

If you do not have your favourite editor yet then I recommend using ◊link[visual-studio-code]{Visual Studio Code}. It's a fully-featured text editor with code highlighting built-in. If there's something it can't handle out of the box then it's quite likely that there's a plugin out there which does exactly what you want.

At the very least I don't want you using regular Notepad on Windows which doesn't even have syntax highlighting. I can't say I know anyone using regular Notepad to write computer programs and I wouldn't recommend this to anyone.

Once you install your chosen editor, we can set up the Ruby interpreter which will allow us to run our programs.