#lang pollen

◊(define-meta title "Getting started")
◊(define-meta subtitle "Gotta go fast")
◊(define-meta published "2021-03-04T02:34:05+0100")
◊(define-meta updated "2021-03-04T02:34:05+0100")
◊(define-meta uuid "a71e77d1-ee45-420e-be8a-de1508316865")

◊(clear-sidenotes)

◊epigraph{
  ◊qt[#:author "Confucius"
      #:src "The Analects, Section 3, Part 15"
      #:url mit-classics]{
    The mechanic, who wishes to do his work well, must first sharpen his tools.
  }
}

◊(define mit-classics
  (x-ref
    "2021-03-04"
    "http://classics.mit.edu/Confucius/analects.3.3.html"
    "The Internet Classics Archive by Daniel C. Stevenson"
  ))

Before starting we need to assemble our tools and confirm that they are working properly. Please don't skip this chapter since it's absolutely essential that you have all the pieces working now so you can focus on understanding programming itself instead of wasting time on fixing your tools.

The tools we will be concerning ourselves with this chapter include:

◊ol{
  ◊li{A text editor}
  ◊li{Ruby interpreter}
  ◊li{A debugger}
}

Don't worry if you're not familiar with any of the tools we will be installing. I'm going to explain their role and how to use them in their respective chapters.

I'll do my best to outline problems you might encounter when trying to install the tools I'm talking about but sometimes you might find yourself with a problem I didn't expect. If that's the case then you should perform a cursory Google search. Should that fail, please contact me so we can try figuring it out together and improving this book.