SHELL := bash
.ONESHELL:
.SHELLFLAGS := -eu -o pipefail -c
.DELETE_ON_ERROR:
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules

ifeq ($(origin .RECIPEPREFIX), undefined)
  $(error This Make does not support .RECIPEPREFIX. Please use GNU Make 4.0 or later)
endif
.RECIPEPREFIX = >

build: css/main.css /tmp/pollen.build

css/main.css: $(shell find sass -type f)
> mkdir -p css/
> sassc -t compressed sass/main.scss > css/main.css

/tmp/pollen.build: $(shell find . -type f)
> raco pollen render
> raco pollen render index.html
> raco pollen publish . /tmp/ruby.guide
> rm -rf public/
> cp -rf /tmp/ruby.guide public
> find ./public -type f -iname \*html | xargs -I {} minify -o {} {}
> touch $@

clean:
> rm -rf *.html
> rm -rf css/
> rm -rf public/
> rm -rf compiled/
> rm -rf /tmp/pollen.build
.PHONY: clean
