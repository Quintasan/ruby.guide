#lang pollen

◊(define-meta title "Introduction")
◊(define-meta subtitle "Congratulations on starting your journey")
◊(define-meta published "2019-04-12T00:00:00+01:00")
◊(define-meta updated "2020-01-15T20:07:32+01:00")
◊(define-meta uuid "b1aaa891-a7c6-4bf4-a01c-4c43c0446abf")

◊(clear-sidenotes)

◊epigraph{
  ◊qt[#:author "Lao Tzu"
      #:src "Dao De Jing, Chapter 64"
      #:url wiki]{
    A journey of a thousand li starts beneath one’s feet
  }
}

◊(define wiki
  (x-ref
    "2020-07-07"
    "https://en.wikipedia.org/wiki/A_journey_of_a_thousand_miles_begins_with_a_single_step"
    "Wikipedia entry for A journey of a thousand miles begins with a single step"
    ))

I would like to congratulate you on taking your first step towards learning programming. Perhaps your're unsure if programming is for you, or if Ruby is a good language to start with. You might have some other questions but now I'm going to address only those two questions since most people ask them.◊mn{further-questions}

◊ndef["further-questions"]{
  If you feel like you have an important question that must be answered in the Introduction, contact me and we can discuss it further.
}

◊subhead{Is programming for me?}

It is my belief that programming can be understood by most people on a decent level. You don't need to finish college or have a Ph. D. in applied mathematics to be a programmer◊mn{exceptions}. For this book, you should be perfectly fine if you know basic arithmetic and logical operators (negation, conjunction, disjunction and sometimes exclusive disjunction). I will try to explain all esoteric knowledge should I introduce any throught the book.

◊ndef["exceptions"]{
  Some companies might require you to have some kind of degree. Working on algebraic effects without understanding category theory might be difficult as well. Fortunately, we will not be doing any of those in this book.
}

At some point you might try comparing yourself towards more experienced programmers and conclude that you're not as good as them and you should stop wasting your time. Please don't do this. I'm convinced this is a bad mindset to have when learining something new and is detrimental to your progress. If you can't resist the urge to compare then at least try finding out what they know that you don't know and note that down for future reference.

It is perfectly okay if you are not the best programmer around - I would say that not being the best has the benefit of being able to learn something new which is always excting. It is also perfectly fine if you are not passionate about programming - I know many programmers working 8 hours a day and spending their free time on something else. You don't have to attend conferences, meetups, usergroups and whatnot. If you want to, that's fine but don't let anyone pressure you into doing those things because ◊i{good} programmers do so.

◊subhead{Is Ruby a good language to begin with?}

I am absolutely tempted to answer with a resounding yes and leaving it at that but let me explain with a code sample:

◊(code-hl "ruby" "scripts/5_times.rb")

I think that you can guess what will this program do right away without much assistance. Ruby is a programming language with a focus on simplicity and productivity. Ruby's slogan is ◊i{A Programmer's Best Friend} which I think best captures the intent of it's creator. You can start programming in ◊strong{any} programming language but I think Ruby is best suited when learning since it really tries it's best to get out of your way.
