#lang pollen

◊(define-meta title "Installing Ruby")
◊(define-meta subtitle "The essential ingredient")
◊(define-meta published "2019-04-12T00:00:00+01:00")
◊(define-meta updated "2020-01-15T20:07:32+01:00")
◊(define-meta uuid "b1aaa891-a7c6-4bf4-a01c-4c43c0446abf")

◊(clear-sidenotes)

Ruby interpreter is available for all major platforms. By that I mean you can program in Ruby using Windows, Mac OS or your Linux distribution of choice. At the time of writing this chapter, the latest Ruby version is 3.0.0. If you're starting out then the exact version doesn't really matter so you could use 2.7.2 if 3.0.0 is not available on your platform.

I'm not going to cover the installation proccess in detail but I'm going to give you some tips on installing Ruby.

◊subhead{Windows}

◊(define RubyInstaller
  (x-ref
    "2021-03-30"
    "https://rubyinstaller.org/"
    "RubyInstaller website"))

On Windows, I recommend using ◊link[RubyInstaller]{RubyInstaller} to install Ruby. Make sure to install the Ruby+DevKit package which will allow us to install gems (Ruby name for libraries created by other people) without too much fuss.

◊subhead{Mac OS}

◊(define homebrew
  (x-ref
    "2020-08-09"
    "https://brew.sh/"
    "Homebrew website"))

You should use ◊link[homebrew]{Homebrew} to install ◊link[asdf]{asdf} on MacOS. Once you have asdf installed you can install any Ruby version using the instructions below:

◊(require pollen/unstable/pygments)
◊(provide highlight)
◊highlight['bash #:line-numbers? #f]{
quintasan@demonbane:~$ asdf plugin add ruby
quintasan@demonbane:~$ asdf install ruby 2.7.2
}

◊subhead{Linux}

◊(define asdf
  (x-ref
    "2021-03-30"
    "https://asdf-vm.com/"
    "asdf version manager documentation"))

Use ◊link[asdf]{asdf} on Linux to install any Ruby version. Once you have asdf installed you should be able to install any Ruby version by issuing the following commands:

◊highlight['bash #:line-numbers? #f]{
quintasan@demonbane:~$ asdf plugin add ruby
quintasan@demonbane:~$ asdf install ruby 2.7.2
}

◊subhead{Verifying installation}

We can verify that we have installed Ruby successfuly by asking the interpreter to tell us it's version:

◊highlight['bash #:line-numbers? #f]{
quintasan@demonbane:~$ ruby --version
ruby 2.7.2p137 (2020-10-01 revision 5445e04352) [x86_64-linux]
}

There's only one more thing we need to install to get rolling really quickly - a proper debugger.